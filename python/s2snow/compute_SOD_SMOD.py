#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (C) 2005-2019 Centre National d'Etudes Spatiales (CNES)
#
# This file is part of Let-it-snow (LIS)
#
#     https://gitlab.orfeo-toolbox.org/remote_modules/let-it-snow
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

import rasterio
import numpy as np
import sys, os 
import logging
from importlib.metadata import version
from s2snow.lis_constant import BLOCKSIZE, COMPRESS_METHOD, SIZE_WINDOW, TILED


def frequent_func(z):
    if z[-1] < z.shape[0] - 10:
        freq = np.bincount(z)
        most_freq = freq.argmax()
        index_start = np.where(z == most_freq)[0][0]
        index_end = index_start + freq[most_freq] - 1
        return [index_start + 1*(most_freq > 0), index_end]
    return [0, 0]


def compute_SOD_SMOD(input_file, synthesis_name=None, output_dir=None):
    """
    Computes the snow onset date (SOD) and the snow melt-out date (SMOD) from a stack of daily snow maps.
    The dates are given in number of days since the first day of the synthesis (usually September 01).
    :param output_dir : output directory
    :param input_file: the interpolated daily raster
    :param synthesis_name: synthesis_name
    :return:
    """
    logging.info("Start compute_SOD_SMOD.py using: {}".format(input_file))
    logging.debug("compute_SOD_SMOD")
    logging.debug("tmp_dir : %s", output_dir)
    logging.debug("input_file : %s", input_file)
    logging.debug("synthesis_name : %s", synthesis_name.format("XXX"))
    
    if not os.path.isfile(input_file):
        msg = "Input file does not exist : {}".format(input_file)
        logging.error(msg)
        raise IOError(msg)
    
    if output_dir is None:
        output_dir = os.path.split(os.path.split(input_file)[0])[0]
    if synthesis_name is None:
        synthesis_id = os.path.split(input_file)[1]
        sod_file = os.path.join(output_dir, "LIS_SNOW-SOD_{}.tif".format(synthesis_id))
        smod_file = os.path.join(output_dir, "LIS_SNOW-SMOD_{}.tif".format(synthesis_id))
    else:
        sod_file = os.path.join(output_dir, synthesis_name.format("SOD"))
        smod_file = os.path.join(output_dir, synthesis_name.format("SMOD"))

    # Open dataset
    src = rasterio.open(input_file, 'r')
    bands = src.meta["count"]
    profile = src.profile
    
    n = profile["height"]
    m = profile["width"]
    sod = np.zeros((n, m), dtype='uint16')
    smod = np.zeros((n, m), dtype='uint16')
    
    # Calculation
    i = 0
    while i < n:
        j = 0
        height = min(n-i, SIZE_WINDOW)
        while j < m:
            width = min(m-j, SIZE_WINDOW)         
            W = src.read(range(1, bands + 1), window=rasterio.windows.Window(j, i, width, height))
            W_cumsum = np.cumsum((W == 0), axis=0, dtype=np.uint16)
            res = np.apply_along_axis(frequent_func, 0, W_cumsum)
            sod[i:i+height, j:j+width] = res[0]
            smod[i:i+height, j:j+width] = res[1]
            j += SIZE_WINDOW
        i += SIZE_WINDOW
    
    # Close dataset
    src.close()
    del src

    # Write output files
    with rasterio.Env():
        profile.update(
            dtype=rasterio.uint16,
            count=1,
            compress=COMPRESS_METHOD,
            tiled=(TILED=="YES"),
            blockxsize=BLOCKSIZE,
            blockysize=BLOCKSIZE)
        
        with rasterio.open(smod_file, 'w', **profile) as dst:
            dst.write(smod.astype(rasterio.uint16), 1)

        with rasterio.open(sod_file, 'w', **profile) as dst:
            dst.write(sod.astype(rasterio.uint16), 1)

            

def show_help():
    """
    Show help for compute_SOD_SMOD .
    :return:
    """
    print("This script is used to compute SOD and SMOD. " \
          + "Input file is the interpolated daily raster generated using run_show_annual_map script." \
          + " Example : DAILY_SNOW_MASKS_T31TCH_20160901_20170831.tif")
    print(
        "Usage: python compute_SOD_SMOD.py DAILY_SNOW_MASKS_T31TCH_20160901_20170831.tif T31TCH_20160901_20170831 /tmp")
    print("Example: python compute_SOD_SMOD.py input_file synthesis_id output_dir")
    print("python compute_SOD_SMOD.py version to show version")
    print("python compute_SOD_SMOD.py help to show help")


def show_version():
    """
    Show LIS version.
    :return:
    """
    print("LIS Version : {}".format(version('s2snow')))


def main(argv):
    compute_SOD_SMOD(*argv[1:])


if __name__ == "__main__":
    if len(sys.argv) < 1 or len(sys.argv) > 3:
        show_help()
    else:
        if sys.argv[1] == "version":
            show_version()
        elif sys.argv[1] == "help":
            show_help()
        else:
            main(sys.argv)
