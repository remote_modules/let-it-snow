# How to use this Dockerfile

## Retrieve OTB 8
OTB and its dependances can be retrieved at https://www.orfeo-toolbox.org/
We need :
* otb.tar.gz 
* SuperBuild-archives.tar.bz2 

## Retrieve LIS
LIS can be retrieve from this repository by git clone and compress into the Docker context as lis.tar.gz

## Build
`docker build -t lis .`

## Run 
`docker run --rm -it lis`

or define an entrypoint;

## Validation

You can validate the docker image created with the tests available in the "docker/tests" folder.

There is actually four tests :
- FSC snow coverage on l8
- FSC snow coverage on S2 with classic hillshade method
- FSC snow coverage on S2 with rastertools
- Snow synthesis on one month

To launch a test : `docker run -v ./docker/tests:/tests -v __input_data_folder__:/input-data -v __baseline_folder__:/baseline lis /tests/tests-<name_test>.sh /input-data /baseline`

The input data folder and baseline folder must be the same folders used for LIS validation tests.