#!/bin/bash -u
# Usage: test-fsc-snow-l8.sh <data-test> <baseline>

DATA_TEST=$1
BASELINE=$2

test_name="l8"

# Cleanup
rm -rf ${test_name}

# Run test
let_it_snow_fsc.py \
  -c ${DATA_TEST}/Landsat/lis_configuration.json \
  -i ${DATA_TEST}/Landsat/LANDSAT8_OLITIRS_XS_20170308_N2A_France-MetropoleD0004H0001 \
  -d ${DATA_TEST}/Landsat/LANDSAT8_OLITIRS_XS_20170308_N2A_France-MetropoleD0004H0001/SRTM/dem.tif \
  -o ${test_name}


# Output comparison
errors=0

echo "-- Comparison pass1"
gdalcompare.py ${BASELINE}/l8_test/pass1.tif ${test_name}/tmp/snow_pass1.tif
errors=$((errors+$?))

echo "-- Comparison pass2"
gdalcompare.py ${BASELINE}/l8_test/pass2.tif ${test_name}/tmp/snow_pass2.tif
errors=$((errors+$?))

echo "-- Comparison pass3"
gdalcompare.py ${BASELINE}/l8_test/pass3.tif ${test_name}/tmp/snow_pass3.tif
errors=$((errors+$?))

echo "-- Comparison snow_all"
gdalcompare.py ${BASELINE}/l8_test/snow_all.tif ${test_name}/tmp/LIS_SNOW_ALL.TIF
errors=$((errors+$?))

echo "-- Comparison final_mask"
gdalcompare.py ${BASELINE}/l8_test/final_mask.tif ${test_name}/tmp/LIS_SEB.TIF
errors=$((errors+$?))

exit ${errors}


