#!/bin/bash -u
# Usage: test-synthesis.sh <data-test> <baseline>

DATA_TEST=$1
BASELINE=$2

test_name="snow_synthesis"
version="1.12.0"

# Cleanup
rm -rf ${test_name}

# Run test
let_it_snow_synthesis.py \
  -i ${DATA_TEST}/SNOW_PRODUCTS/SENTINEL2A_20180101-105435-457_L2A_T31TCH_D_V1-4 \
  -i ${DATA_TEST}/SNOW_PRODUCTS/SENTINEL2A_20180131-105416-437_L2A_T31TCH_D_V1-4 \
  -d ${DATA_TEST}/SNOW_PRODUCTS/LANDSAT8-OLITIRS-XS_20180115-103629-617_L2A_T31TCH_D_V1-9 \
  -d ${DATA_TEST}/SNOW_PRODUCTS/LANDSAT8-OLITIRS-XS_20180131-103619-890_L2A_T31TCH_D_V1-9 \
  -c ${DATA_TEST}/SYNTHESIS/synthesis_configuration.json \
  -j ${DATA_TEST}/SYNTHESIS/synthesis_launch.json \
  -o ${test_name} \
  -V ${version}


# Output comparison
errors=0

echo "-- Comparison SCD"
gdalcompare.py \
  ${BASELINE}/snow_synthesis_test/LIS_S2L8-SNOW-SCD_T31TCH_20180101_20180131_${version}_1.tif \
  ${test_name}/LIS_S2L8-SNOW-SCD_T31TCH_20180101_20180131_${version}_1.tif
errors=$((errors+$?))

echo "-- Comparison NOBS"
gdalcompare.py \
  ${BASELINE}/snow_synthesis_test/LIS_S2L8-SNOW-NOBS_T31TCH_20180101_20180131_${version}_1.tif \
  ${test_name}/LIS_S2L8-SNOW-NOBS_T31TCH_20180101_20180131_${version}_1.tif
errors=$((errors+$?))

echo "-- Comparison SOD"
gdalcompare.py \
  ${BASELINE}/snow_synthesis_test/LIS_S2L8-SNOW-SOD_T31TCH_20180101_20180131_${version}_1.tif \
  ${test_name}/LIS_S2L8-SNOW-SOD_T31TCH_20180101_20180131_${version}_1.tif
errors=$((errors+$?))

echo "-- Comparison SMOD"
gdalcompare.py \
  ${BASELINE}/snow_synthesis_test/LIS_S2L8-SNOW-SMOD_T31TCH_20180101_20180131_${version}_1.tif \
  ${test_name}/LIS_S2L8-SNOW-SMOD_T31TCH_20180101_20180131_${version}_1.tif
errors=$((errors+$?))

echo "-- Comparison NSP"
gdalcompare.py \
  ${BASELINE}/snow_synthesis_test/LIS_S2L8-SNOW-NSP_T31TCH_20180101_20180131_${version}_1.tif \
  ${test_name}/LIS_S2L8-SNOW-NSP_T31TCH_20180101_20180131_${version}_1.tif
errors=$((errors+$?))

echo "-- Comparison CCD"
gdalcompare.py \
  ${BASELINE}/snow_synthesis_test/CLOUD_OCCURENCE_T31TCH_20180101_20180131.tif \
  ${test_name}/tmp/CLOUD_OCCURENCE_T31TCH_20180101_20180131.tif
errors=$((errors+$?))

exit ${errors}
