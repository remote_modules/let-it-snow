#!/bin/bash -u
# Usage: test-fsc-snow-s2-hillshade.sh <data-test> <baseline>

DATA_TEST=$1
BASELINE=$2

test_name="shaded_snow_no_relief_shadow_mask"
version="1.12.0"

# Cleanup
rm -rf ${test_name}

# Run test
let_it_snow_fsc.py \
  -c ${DATA_TEST}/S2/shaded_snow/global_parameters_shaded_snow.json \
  -i ${DATA_TEST}/S2/SENTINEL2B_20210124-103807-070_L2A_T31TGM_C_V1-0 \
  -d ${DATA_TEST}/S2/SENTINEL2B_20210124-103807-070_L2A_T31TGM_C_V1-0/Leman_dem_merged.tif \
  -o ${test_name} \
  -V ${version}


# Output comparison
errors=0

echo "-- Comparison FSC TOC"
gdalcompare.py \
  ${BASELINE}/shaded_snow_test/shaded_snow_no_relief_shadow_mask/LIS_S2-SNOW-FSC-TOC_T31TGM_20210124T103807_${version}.tif \
  ${test_name}/LIS_S2-SNOW-FSC-TOC_T31TGM_20210124T103807_${version}.tif

errors=$((errors+$?))
exit ${errors}

